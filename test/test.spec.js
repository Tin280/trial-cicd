const { Builder ,Browser, WebDriver, By, Key} = require("selenium-webdriver");

const waitSeconds = seconds => new Promise(resovle => {
    setTimeout(() => resovle(), seconds * 1000);
})

describe.skip("Google Search", () => {
    /** @type {WebDriver} */
    let driver;
    beforeAll(async () => {
        driver = await new Builder().forBrowser(Browser.FIREFOX).build();
    });

    afterAll(async () => {
        await driver.quit();
    });

    test("Can do Google Search", async () => {
        await driver.get("http://google.com");
        await driver.findElement(By.id("W0wltc")).click();
        const searcher = await driver.findElement(By.id("APjFqb"));
        searcher.sendKeys("LAB",Key.ENTER);
        await waitSeconds(4);
    },15000);
    // test.only("test Color",async () => {
    //     await driver.get("https://ponet.fi/projects/color-picker/")
    //     await driver.findElement(By.id("slider-g"))
    // })
});
