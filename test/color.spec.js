const { Builder ,Browser, WebDriver, By, Key} = require("selenium-webdriver");
const waitSeconds = seconds => new Promise(resovle => {
    setTimeout(() => resovle(), seconds * 1000);
})

const BASE_URL = "https://ponet.fi/projects/color-picker/"

describe("Color UI", () => {
    /** @type {WebDriver} */
    let driver;
    beforeAll(async () => {
        driver = await new Builder().forBrowser(Browser.FIREFOX).build();
    },10000);
    afterEach(async () => {
        await waitSeconds(0.01);
    },10000)
    afterAll(async () => {
        // await waitSeconds(10);
        await driver.close();
    },10000);

    test("Can navigate to the site", async () => {
        await driver.get(BASE_URL);
        
    },10000);
    test("Can slide the RED slider", async() => {
        const redSlider = await driver.findElement(By.id("slider-r"));
        const numberOfPresses = 300;
        
        // Use a loop to simulate pressing the right arrow key
        for (let i = 0; i < numberOfPresses; i++) {
            await redSlider.sendKeys(Key.RIGHT);
            await waitSeconds(0.01)
        }
        // await driver.executeScript("arguments[0].value = 10;", redSlider);
        // const valueInput = await driver.findElement(By.id("value-r"));
        // await valueInput.sendKeys("10");
        },15000)
    test("Can slide the Green slider", async () => {
        const slider = await driver.findElement(By.id("slider-g"));
         //const press_left = Array(128).fill(Key.LEFT);
         const press_left = Key.LEFT.repeat(128)
        slider.sendKeys(press_left);
    },15000)
    test("Can read the hex value after the tests and its arrange", async() => {
        // #ff7f00
        const hexfield = await driver.findElement(By.id("value-hex"))
        const hexValue = await hexfield.getAttribute("value")
        expect(hexValue).toEqual('#ff7f00')
    },15000)
});
