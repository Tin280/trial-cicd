#!/bin/bash

git init
npm init -y

touch .env.example

cat <<EDF >> ".env.example"
API_URL=<http://example.com/cplor-picker

EDF

touch .gitignore
echo "node_modules" >> .gitignore
echo ".DS_Store" >> .gitignore
echo ".env" >> .gitignore

mkdir test
touch test/test.spec.js
touch test/color.spec.js

npm i --save-dev jest supertest selenium-webdriver mocha chai

touch readme.md

